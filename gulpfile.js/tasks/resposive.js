const gulp = require('gulp');
const log = require('fancy-log');
const responsive = require('gulp-responsive-images'); // Install in Unix `sudo apt-get install graphicsmagick`
const imagemin = require('gulp-imagemin');
const colors = require('ansi-colors');
const count = require('gulp-count');

// load config
const config = require('../config');

const task = () => gulp.src(config.responsive.sourceFiles)

  .pipe(responsive({
    // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
    'otros-productos/*.{jpg,png,jpeg,gif,webp}': [{
        width: 290,
        rename: { suffix: '-290px' },
    }],
    'materiales-electricos/*.{jpg,png,jpeg,gif,webp}': [
      {
        width: 290,
        rename: { suffix: '-290px' },
      },
      {
        width: 464,
        rename: { suffix: '' },
      }
    ],
    'automatizacion-y-control/*.{jpg,png,jpeg,gif,webp}': [{
      width: 290,
      rename: { suffix: '-290px' },
    }],
    'cables-electricos/*.{jpg,png,jpeg,gif,webp}': [{
      width: 290,
      rename: { suffix: '-290px' },
    }],
    'cajas-electricas/*.{jpg,png,jpeg,gif,webp}': [{
      width: 290,
      rename: { suffix: '-290px' },
    }],
    'luminarias-e-iluminacion/*.{jpg,png,jpeg,gif,webp}': [{
      width: 290,
      rename: { suffix: '-290px' },
    }],
    'present-gallery/*.{jpg,png,jpeg,gif,webp}': [
      {
        height: 524,
        rename: { suffix: '-524px' },
      },
      {
        height: 170,
        rename: { suffix: '-170px' },
      }
    ]
  }, {
      // Global configuration for all images
      // The output quality for JPEG, WebP and TIFF output formats
      quality: 70,
      // Use progressive (interlace) scan for JPEG and PNG output
      progressive: true,
      // Strip all metadata
      withMetadata: false,
    }))
  // minify (production)
  .pipe(imagemin([
    // plugins (https://www.npmjs.com/browse/keyword/imageminplugin)
    imagemin.gifsicle(),
    imagemin.jpegtran(),
    imagemin.optipng(),
    // imagemin.svgo()
  ], {
      // options
      verbose: true
    }))

  // log
  .pipe(count({
    message: colors.white('Responsive image files processed: <%= counter %>'),
    logger: (message) => log(message)
  }))

  // save
  .pipe(gulp.dest(config.responsive.destinationFolder));

gulp.task('responsive', task);
module.exports = task;