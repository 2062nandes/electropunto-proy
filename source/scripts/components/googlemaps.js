/*
  |--------------------------------------------------------------------------
  | Google Maps Javascript API
  |--------------------------------------------------------------------------
  | Add the api script of google maps with your ´YOUR_API_KEY´ before the script.js from project
  | Example Pug: ´script(src= "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY")´
  |
  */
export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data() {
    return {
      mapName: 'map',
      zoom: 17,
      center: {
        latitude: -17.773491,
        longitude: -63.162315
      },
      markerCoordinates: [
        {
          latitude: -17.773491,
          longitude: -63.162315,
          title: `<div id="content">
                      <h2 id="firstHeading" class="firstHeading">ELECTRO PUNTO - BOLIVIA</h2>
                      <p>Venta de Materiales Eléctricos en General</p>
                      <div id="bodyContent">
                        <p><b>Dirección:</b> Santa Cruz: 
                          Av. Paraguá # 2350 
                          <br>esq. De Cáceres (entre 2º y 3º Anillo)
                          <br><b>Cel.:</b> 76040672
                          <br><b>Telfs.:</b>  3263551
                          <br>electropunto@electro-punto.com
                          <br>jorgevallejos@electro-punto.com
                        </p>  
                      </div><hr>
                      <footer><a class="button--cta center" href="https://www.google.com/maps/place/17%C2%B046'24.6%22S+63%C2%B009'44.3%22W/@-17.773491,-63.164509,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d-17.773491!4d-63.162315" target="_blank">Ver en google maps</a></footer>
                    </div>`
        }
      ]
    }
  },
  mounted() {
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: this.zoom,
      center: new google.maps.LatLng(this.center.latitude, this.center.longitude)
    }
    const map = new google.maps.Map(element, options);

    this.markerCoordinates.forEach((coord) => {
      const position = new google.maps.LatLng(coord.latitude, coord.longitude);
      const marker = new google.maps.Marker({
        position,
        map
      });

      const infowindow = new google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close();
      });
      google.maps.event.addListener(marker, 'click',
        (function (marker, coord, infowindow) {
          return function () {
            infowindow.setContent(coord.title);
            infowindow.open(map, marker);
          };
        })(marker, coord, infowindow));
    });
    // matchMedia('')
  }
}