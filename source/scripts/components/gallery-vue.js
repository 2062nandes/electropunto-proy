import { mainmenu } from "../data/menus"
import { path_media, path_page } from "../data/routes"
export const galleryVue = {
  template: `<section class="rex-item s-75 gallery l-block" id="gallery">
              <template v-for="item in mainmenu[idmenu].list">
                <div class="gallery-item" v-for="subitem in item.prod" v-if="subitem.src != '' && subitem.title == ''">
                  <div class="card card--course">
                    <div class="card__img"><img class="image--contain" :src="path_media + 'images/' + mainmenu[idmenu].href + '/' + subitem.src" :alt="item.title" ></div>
                    <!--<div class="card__content">
                      <div class="card__title" v-text="item.title"></div>
                      <footer class="card__footer">
                        <div class="card__user">
                          <div class="card__user-name" v-text="subitem.title"></div>
                        </div>
                      </footer>
                    </div> -->
                  </div>
                </div>
              </template>
            </section>`,
  data() {
    return {
      path_page,
      path_media,
      mainmenu
    }
  },
  props: {
    idmenu: {
      required: true
    }
  }
}