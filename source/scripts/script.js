import { menuToggle } from "./modules/menu";
import { edTabs } from "./modules/tabs";
import { date, getdate } from "./modules/date";
// import { masonryLayout } from "./modules/masonry-layout";

//Data Vue
import { mainmenu, menuinicio } from "./data/menus"
import { path_media, path_page } from "./data/routes"
import { contactform } from "./data/contact-form"

// import Vue from 'vue/dist/vue.min'
import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

// Vue Components
import { googleMap } from './components/googlemaps'
import { galleryVue } from './components/gallery-vue'
Vue.component('google-map', googleMap)
Vue.component('gallery-vue', galleryVue )

const rex = new Vue({
  el: '#rex',
  data: {
    path_media,
    path_page,
    menuinicio,
    mainmenu,
    formSubmitted: false,
    vue: contactform,
    slideshow: {
      title: '',
      href_id: '',
      src: '',
      description: ''
    }
  },
  created: function () {
    this.slideshow.src = this.path_media + 'images/present-gallery/' + 'slide01' + '-524px.jpg'
    this.slideshow.title = this.mainmenu[0].title
    this.slideshow.description = ''
    this.slideshow.href_id = 0
  },
  mounted: function () {
    window.edTabs = edTabs;
    menuToggle();
    getdate();
    // masonryLayout(document.getElementById('gallery'),
    //   document.querySelectorAll('.gallery-item'), 3)
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    selectGallery: function( id, url, description ) {
      this.slideshow.src = this.path_media + 'images/present-gallery/' + url + '-524px.jpg'
      this.slideshow.title = this.mainmenu[id].title
      this.slideshow.description = description
      this.slideshow.href_id = id
    }
  }
})