export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-users'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-envelope'
  }
]

export const mainmenu = [
  {
    title: 'Materiales eléctricos',
    href: 'materiales-electricos',
    list: [
      { 
        title: 'METALTEX',
        href: '#',
        prod: [
          {
            title: '',
            src: 'metaltex-290px.jpg'
          },
          { 
            title: 'Cajas IP65 para botoneras',
            src: ''
          },
          { 
            title: 'Contactores monofásicos y trifásicos',
            src: 'contador tripolar-290px.jpg'
          },
          { 
            title: 'Reles estado solido',
            src: ''
          },
          { 
            title: 'Reles térmicos',
            src: ''
          },
          { 
            title: 'Sensores inductivos y capacitivos',
            src: ''
          },
          { 
            title: 'Temporizadores',
            src: 'Temporizadores -1-290px.jpg'
          }
        ]
      },        
      { 
        title: 'MARGIRIUS',
        href: '#',
        prod: [
          {
            title: '',
            src: 'margirius-290px.jpg'
          },
          { 
            title: 'Interruptores',
            src: 'interruptores de tecla -2-290px.jpg'
          },
          { 
            title: 'Interruptores de tecla',
            src: 'interruptores de tecla -3-290px.jpg'
          },
          { 
            title: 'Reactancias electrónicas',
            src: ''
          },
          { 
            title: 'Sensores de movimiento',
            src: ''
          },
          { 
            title: 'Supresores de pico 2002',
              src: ''
          }
        ]
      },
      {
        title: 'STANLEY',
        href: '#',
        prod: [
          {
            title: '',
            src: 'stanley-290px.jpg'
          },
          { 
            title: 'Alicates',
            src: 'alicates -1-290px.jpg'
          },
          { 
            title: 'Destornilladores',
            src: 'destornilladores -2-290px.jpg'
          }
        ]
      },      
      { 
        title: 'CAMSCO',
        href: '#',
        prod: [
          {
            title: '',
            src: 'camsco-290px.jpg'
          },
          { 
            title: 'Aisladores Poxis para barras',
            src: ''
          },
          { 
            title: 'Amperímetros analógicos y digital',
            src: 'Amperimetros-290px.png'
          },
          { 
            title: 'Balizas de movilidad',
            src: ''
          },
          { 
            title: 'Balizas fijas',
            src: ''
          },
          { 
            title: 'Botoneras de potencia',
            src: ''
          },
          { 
            title: 'Botoneras focos',
            src: ''
          },
          { 
            title: 'Enchufes industriales',
            src: ''
          },
          { 
            title: 'Interruptores de pedal',
            src: ''
          },
          { 
            title: 'Llaves selectoras',
            src: ''
          },
          { 
            title: 'Sirenas de turbinas',
            src: ''
          },
          { 
            title: 'Sirenas electrónicas',
            src: ''
          },
          { 
            title: 'Temporizadores',
            src: 'Temporizadores-290px.jpg'
          },
          { 
            title: 'Terminales',
            src: ''
          },
          { 
            title: 'Terminales aislados',
            src: 'terminales aislados -3-290px.jpg'
          },
          { 
            title: 'Termocuplas',
            src: ''
          },
          { 
            title: 'Termostatos',
            src: ''
          },
          { 
            title: 'Ventiladores cooler',
            src: ''
          },
          { 
            title: 'Voltímetros digitales y analógicos',
            src: ''
            // src: 'voltimetro digital-290px.jpg'
          }
        ]
      },
      { 
        title: 'PANASONIC',
        href: '#',
        prod: [
          {
            title: '',
            src: 'panasonic-290px.jpg'
          },
          { 
            title: 'Baterías',
            src: ''
          },
          { 
            title: 'Pilas',
            src: ''
          }
        ]
      },
      {
        title: 'TIERRA GEL',
        href: '#',
        prod: [
          {
            title: '',
            src: 'tierra-gel-290px.jpg'
          },
          {
            title: 'Sales electronicas',
            src: ''
          }
        ]
      }
    ]
  },
  {
    title: 'Cables eléctricos',
    href: 'cables-electricos',
    list: [
      { 
        title: 'INDUSCABOS',
        href: '#',
        prod: [
          {
            title: '',
            src: 'induscabos-290px.jpg'
          }, 
          { 
            title: 'Cables de aluminio aislados cuádruplex duplex',
            src: ''
          },
          { 
            title: 'Cables de aluminio desnudos', 
            src: 'Cables de acero desnudos -1-290px.jpg'
          },
          { 
            title: 'Cables monopolares, bipolares, tripolares, tetrapolares', 
            src: 'cables tripolares -1-290px.jpg'
          },
          { 
            title: 'Cordones bicolor', 
            src: ''
          },
          { 
            title: 'Cordones blancos',
            src: ''
          }
        ]
      },
      { 
        title: '3M',
        href: '#',
        prod: [
          {
            title: '',
            src: '3m-290px.jpg'
          }, 
          { 
            title: 'Cables UTP y FTP',
            src: 'cables FTP -1-290px.jpg'
          },
          { 
            title: 'Cintas de espumas',
            src: ''
          },
          { 
            title: 'Cintas marcadoras de pavimento',
            src: 'cinta marcadora de pavimento-290px.jpg'
          },
          { 
            title: 'Limpia contactos eléctricos',
            src: ''
          },
          { 
            title: 'Líquidos desengrasantes',
            src: 'liquido desengrasante 3M-290px.jpg'
          },
          { 
            title: 'Muflas',
            src: ''
          }
        ]
      },
      {
        title: 'Cordeiro',
        href: '#',
        prod: [
          {
            title: '',
            src: 'cordeiro-290px.jpg'
          },
          {
            title: '',
            src: ''
          },
        ]
      },
      // { 
      //   title: 'METALCO',
      //   href: '#',
      //   prod: [  
      //     { 
      //       title: 'Codos EMT',
      //       src: 'Conector EMT-1-290px.jpg' 
      //     },
      //     { 
      //       title: 'Conectores EMT',
      //       src: 'Conector EMT-290px.jpg' 
      //     },
      //     { 
      //       title: 'Tubos metálicos',
      //       src: 'tubos metalicos -1-290px.jpg'
      //      }
      //   ]
      // },
      // { 
      //   title: 'TIPE',
      //   href: '#',
      //   prod: [
      //     { 
      //       title: 'Codos antiexplosivos',
      //       src: '' 
      //     },
      //     { 
      //       title: 'Tubos antiexplosivos',
      //       src: ''
      //      }
      //   ]
      // }
    ]
  },
  {
    title: 'Automatización y control',
    href: 'automatizacion-y-control',
    list: [
      { 
        title: 'ABB',
        href: '#',
        prod: [
          {
            title: '',
            src: 'abb-290px.jpg'
          },
          { 
            title: 'Arrancadores suaves',
            src: 'arrancador-suave-1-290px.jpg'
          },
          { 
            title: 'Contactores',
            src: ''
          },
          { 
            title: 'Fuentes de alimentación',
            src: 'fuentes de alimentación-290px.jpg'
          },
          { 
            title: 'Gabinetes metálicos',
            src: ''
          },
          { 
            title: 'Mando y maniobra',
            src: 'mando y maniobra ABB-290px.jpg'
          },
          { 
            title: 'Reles térmicos',
            src: 'rele termico ABB-290px.jpg'
          },
          { 
            title: 'Variadores eléctricos',
            src: ''
          },
        ]
      },
      { 
        title: 'TOGAMI',
        href: '#',
        prod: [
          {
            title: '',
            src: 'togami-290px.jpg'
          },
          { 
            title: 'Arrancadores directos',
            src: ''
          },
          { 
            title: 'Contactores',
            src: ''
          },
          { 
            title: 'Reles térmicos',
            src: ''
          }
        ]
      },
      { 
        title: 'TRAMONTINA',
        href: '#',
        prod: [
          {
            title: '',
            src: 'tramontina-290px.jpg'
          },
          { 
            title: 'Cajas condulet tipo T- X – LL- LB – LR',
            src: 'cajas condulet tipo T-290px.jpg'
          },
          { 
            title: 'Cajas de paso',
            src: ''
          }
        ]
      },
      { 
        title: 'VETO',
        href: '#',
        prod: [
          {
            title: '',
            src: 'veto-290px.jpg'
          },
          { 
            title: 'Placas doble mixta',
            src: ''
          },
          { 
            title: 'Soquet de porcelanas',
            src: ''
          }
        ]
      },
      {
        title: 'TUBOS CONDUIT PIPE - EMT',
        href: '#',
        prod: [
          {
            title: '',
            src: 'conduit-290px.jpg'
          },
        ]
      },
      {
        title: 'WEG',
        href: '#',
        prod: [
          {
            title: '',
            src: 'weg-290px.jpg'
          },
        ]
      }
      // { 
      //   title: 'OSMAR',
      //   href: '#',
      //   prod: [
      //     { 
      //       title: 'Luminarias spot de sobreponer',
      //       src: 'Luminarias spot de sobreponer-290px.jpg'
      //     },
      //     { 
      //       title: 'Placas doble mixta',
      //       src: ''
      //     }
      //   ]
      // },
    ]
  },
  {
    title: 'Luminarias e iluminación',
    href: 'luminarias-e-iluminacion',
    list: [
      { 
        title: 'YASHIPS',
        href: '#',
        prod: [
          {
            title: '',
            src: 'yaships-290px.jpg'
          },
          { 
            title: 'Iluminación tipo campanas',
            src: ''
          },
          { 
            title: 'Luminarias acrílicas',
            src: ''
          },
          { 
            title: 'Luminarias públicas',
            src: ''
          }
        ]
      },
      { 
        title: 'OSRAM',
        href: '#',
        prod: [
          {
            title: '',
            src: 'osram-290px.jpg'
          },
          { 
            title: 'Focos ovoide',
            src: 'focos ovoide-290px.jpg'
          },
          { 
            title: 'Focos tubulares',
            src: ''
          },
          { 
            title: 'Lámparas vapor de mercurio',
            src: 'lampara vapor mercurio-290px.jpg'
          },
          { 
            title: 'Lámparas vapor de sodio',
            src: ''
          },
          { 
            title: 'Lámparas vapor metálico',
            src: ''
          }
        ]
      },
      { 
        title: 'OPPLE',
        href: '#',
        prod: [
          {
            title: '',
            src: 'opple-290px.jpg'
          },
          { 
            title: 'Iluminación tipo campanas',
            src: 'iluminacion tipo campana-290px.jpg'
          },
          { 
            title: 'Luminarias de sobre poner',
            src: 'luminarias acrilicas -1-290px.jpg'
          },
          { 
            title: 'Luminarias spot de empotrar',
            src: 'luminarias acrilicas -3-290px.jpg'
          },
          { 
            title: 'Reflectores',
            src: 'reflectores opple-290px.jpg'
          }
        ]
      },
      { 
        title: 'TASCHIBRA',
        href: '#',
        prod: [
          {
            title: '',
            src: 'taschibra-290px.jpg'
          },
          { 
            title: 'Iluminación tipo campanas',
            src: ''
          },
          { 
            title: 'Luminarias de sobre poner',
            src: ''
          },
          { 
            title: 'Luminarias spot de empotrar',
            src: ''
          },
          { 
            title: 'Reflectores',
            src: ''
          }
        ]
      },
      {
        title: 'BRAZOS PARA ALUMBRADO PÚBLICO',
        href: '#',
        prod: [
          {
            title: '',
            src: 'brazos-alumbrado-publico-290px.jpg'
          }         
        ]
      }
    ]
  },
  {
    title: 'Cajas eléctricas',
    href: 'cajas-electricas',
    list: [
      {
        title: '',
        href: ''  
      },
      { 
        title: 'CEMAR',
        href: '#',
        prod: [
          {
            title: '',
            src: 'cemar-290px.jpg'
          },
          { 
            title: 'Cajas con barras',
            src: 'caja de montaje -1-290px.jpg'
          },
          { 
            title: 'Cajas de PVC de montaje',
            src: 'caja de montaje -2-290px.jpg'
          },
          { 
            title: 'Cajas de PVC de paso',
            src: 'caja de pvc de paso-290px.jpg'
          },
          { 
            title: 'Cajas metálicas modulares',
            src: 'cajas metalicas modulares -2-290px.jpg'
          }
        ]
      },
      { 
        title: 'CAJAS METÁLICAS',
        href: '#',
        prod: [
          {
            title: '',
            src: 'cajas-metalicas-290px.jpg'
          }
        ]
      }
    ]
  },
  {
    title: 'Otros productos',
    href: 'otros-productos',
    list: [
      {
        title: 'CRC',
        href: '#',
        prod: [
          {
            title: '',
            src: 'crc-290px.jpg'
          },
          { 
            title: 'Espumas',
            src: ''
          },
          { 
            title: 'Espumas antillamas',
            src: ''
          },
          { 
            title: 'Limpia contactos eléctricos',
            src: ''
          },
          { 
            title: 'Líquidos desengrasantes',
            src: 'liquidos desengrasantes-290px.jpg'
          },
          { 
            title: 'Líquidos Penetrantes',
            src: ''
          },
        ]
      },
      {
        title: 'SOLERA',
        href: '#',
        prod: [
          {
            title: '',
            src: 'solera-290px.jpg'
          },
          { 
            title: 'Cajas de PVC de paso',
            src: ''
          },
          { 
            title: 'Enchufes Shuko',
            src: 'enchufes schuko-290px.jpg'
          },
          { 
            title: 'Temporizadores',
            src: ''
          },
        ]
      },
      {
        title: 'FAMATEL',
        href: '#',
        prod: [
          {
            title: '',
            src: 'famatel-290px.jpg'
          },
          { 
            title: 'Enchufes industriales',
            src: 'enchufes industriales-290px.jpg'
          },
          { 
            title: 'Hembras extensibles',
            src: ''
          },
        ]
      },
      {
        title: 'THERMOWELD',
        href: '#',
        prod: [
          {
            title: '',
            src: 'thermoweld-290px.jpg'
          },
          { 
            title: 'Masillas selladoras',
            src: ''
          },
          { 
            title: 'Moldes',
            src: ''
          },
          { 
            title: 'Soldaduras',
            src: ''
          },
          { 
            title: 'Tapas de aterramientos',
            src: 'tapas de aterramiento-290px.jpg'
          },
        ]
      },
      {
        title: 'INTELLI',
        href: '#',
        prod: [
          {
            title: '',
            src: 'intelli-290px.jpg'
          },
          { 
            title: 'Aisladores bajantes para pararrayos',
            src: ''
          },
          { 
            title: 'Conectores para jabalinas',
            src: 'Conectores para jabalinas -1-290px.jpg'
          },
          { 
            title: 'Cuplas para jabalinas',
            src: 'Conectores para jabalinas -2-290px.jpg'
          },
          { 
            title: 'Jabalinas 5 / 8 y Ÿ',
            src: ''
          },
        ]
      },
      {
        title: 'LORENZETTI',
        href: '#',
        prod: [
          {
            title: '',
            src: 'lorenzetti-290px.jpg'
          },
          { 
            title: 'Duchas maxi',
            src: 'DUCHA-LORENZETTI-MAXI-ULTRA-290px.jpg'
          },
          { 
            title: 'Soquet de porcelanas',
            src: ''
          },
          { 
            title: 'Térmicos monopolares, tripolares y bipolares',
            src: ''
          },
        ]
      },
      {
        title: 'NANSEN',
        href: '#',
        prod: [
          {
            title: '',
            src: 'nansen-290px.jpg'
          },
          { 
            title: 'Medidores monofásicos',
            src: ''
          },
          { 
            title: 'Medidores trifásicos',
            src: ''
          },
        ]
      },
      {
        title: 'KYORITSU',
        href: '#',
        prod: [
          { 
            title: '',
            src: 'kyoritzu-290px.jpg'
          },
          { 
            title: 'Megger de aislamiento',
            src: 'megger de aislamiento-290px.jpg'
          },
          { 
            title: 'Megger de tierra',
            src: ''
          },
          { 
            title: 'Multitester',
            src: 'multitester -1-290px.jpg'
          },
          { 
            title: 'Pinzas amperimétricas',
            src: 'pinza-amperimetrica-1-290px.jpg'
          },
        ]
      },
      {
        title: 'ZOLODA',
        href: '#',
        prod: [
          {
            title: '',
            src: 'zoloda-290px.jpg'
          },
          { 
            title: 'Cablecanal de piso',
            src: 'cable canal de piso-290px.jpg'
          },
          { 
            title: 'Cablecanales liso blanco',
            src: ''
          },
          { 
            title: 'Cablecanales ranurados',
            src: 'cablecanal-ranurado-290px.jpg'
          },
        ]
      },
      {
        title: 'OLIVO',
        href: '#',
        prod: [
          {
            title: '',
            src: 'olivo-290px.jpg'
          },
          { 
            title: 'Pernos máquina',
            src: ''
          },
          { 
            title: 'Platos expansión',
            src: ''
          },
          { 
            title: 'Varilla de anclaje',
            src: ''
          },
        ]
      },
      {
        title: 'MAGNET',
        href: '#',
        prod: [
          {
            title: '',
            src: 'magnet-290px.jpg'
          },
          {
            title: 'Guantes de empalme',
            src: ''
          },
          {
            title: 'Tornillo de clavado',
            src: ''
          },
        ]
      },
      {
        title: 'FERRETERIA EN LÍNEA - INDUSTRIAS',
        href: '#',
        prod: [
          {
            title: '',
            src: 'ferreteria-en-linea-290px.jpg'
          },
          {
            title: 'MECRIL',
          }
        ]
      }
    ]
  }
]